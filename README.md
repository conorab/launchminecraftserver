# launchMinecraftServer

launchMinecraftServer is a Bash script and SystemD service to start and stop your Minecraft server with SystemD and scale the amount of slots with the amount of available R.A.M.

## Usage Instructions

Extract the tarball to /; the SystemD unit will be installed at '/etc/systemd/system/minecraft.service' and the Bash script will be installed at '/usr/local/bin/launchMinecraftServer' with execute permissions already set.

### SystemD Unit Variables

You may want to modify the SystemD unit to match your Minecraft server installation.

* Set the "User" variable to match the user which will run the Minecraft server jar.
* In version 2, by default, your current working directory will be the home folder of that user. In version 3, by default, your current working directory will be "/home/minecraft". This is set in the "WorkingDirectory" variable.
* When the Bash script runs, it will check if the "minecraftJarPath" variable has been set. If not, it will try and run 'minecraft_server.jar'. To change this, replace 'minecraft_server.jar' in the line starting with 'Environment=' with the path to your Minecraft server jar. 

### Bash Script Variables

**reservedRAM**

The amount of R.A.M. in KiB which is reserved for other applications. The total amount of R.A.M. which can be used by Minecraft is taken by deducting this number from the total available R.A.M.

**minimumRAM**

If the total amount of R.A.M. minus the 'reservedRAM' is less than this number (in KiB), do not run the Minecraft server.

**RAMForTwoPlayers**

The amount of R.A.M. (in KiB) required before more than one slot is usable on the server.

**RAMPerPlayer**

The amount of R.A.M. required for each player slot above 2 players.
defaultMinecraftJarPath

The path to the Minecraft server jar that will be used if the 'minecraftJarPath' variable is not set.

**serverPropertiesFilePath**

The Bash script modifies the 'server.properties' file in order to control how many slots the server uses. The path of the 'server.properties' file is set in the 'serverPropertiesFilePath' variable. If your 'server.properties' file is in a different location, just change the path in this variable.

### Dependencies

Ensure the following commands are available:

* screen - The G.N.U. Screen utility.
* screenPushTilExit - Waits for the screen in a SystemD service to close and optionally sends a string on an interval to the screen. 

Core utilities such as 'rm' are assumed. I used Bash version 4.4.12(1)-release but other relatively new versions should be fine as well. 
