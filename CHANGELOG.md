# Change Log

## Version 3

	gpg: Signature made Tue 04 Sep 2018 08:02:39 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

## Version 2

	gpg: Signature made Sun 26 Aug 2018 02:34:27 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

This version is broken and only available for historical purposes.

## Version 1

	gpg: Signature made Sun 15 Dec 2019 22:45:03 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

## Version 0

	gpg: Signature made Sun 15 Dec 2019 22:45:03 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

